package ru.tsc.babeshko.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Task;

public final class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}