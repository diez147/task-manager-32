package ru.tsc.babeshko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.babeshko.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.babeshko.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.babeshko.tm.dto.response.TaskUnbindFromProjectResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

}