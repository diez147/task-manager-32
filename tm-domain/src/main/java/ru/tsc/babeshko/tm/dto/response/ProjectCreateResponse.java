package ru.tsc.babeshko.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Project;

public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final Project project) {
        super(project);
    }

}