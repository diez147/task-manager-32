package ru.tsc.babeshko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.request.AbstractRequest;
import ru.tsc.babeshko.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(@NotNull RQ request);

}