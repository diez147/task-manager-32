package ru.tsc.babeshko.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Task;

public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final Task task) {
        super(task);
    }

}