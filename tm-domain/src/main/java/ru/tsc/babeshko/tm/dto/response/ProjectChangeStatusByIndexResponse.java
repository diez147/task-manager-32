package ru.tsc.babeshko.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Project;

public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}
