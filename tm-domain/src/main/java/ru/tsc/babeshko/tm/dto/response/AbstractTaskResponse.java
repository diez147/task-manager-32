package ru.tsc.babeshko.tm.dto.response;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Task;

@Getter
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private Task task;

    public AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

}