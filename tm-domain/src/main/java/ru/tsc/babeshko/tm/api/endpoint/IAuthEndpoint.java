package ru.tsc.babeshko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.request.UserLoginRequest;
import ru.tsc.babeshko.tm.dto.request.UserLogoutRequest;
import ru.tsc.babeshko.tm.dto.request.UserProfileRequest;
import ru.tsc.babeshko.tm.dto.response.UserLoginResponse;
import ru.tsc.babeshko.tm.dto.response.UserLogoutResponse;
import ru.tsc.babeshko.tm.dto.response.UserProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}